<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class homeController extends Controller
{
    public function index()
    {
    return view('index');
    }

    public function store(Request $value){
        $num1 = $value->post('num');
        $num2 = $value->post('num2');
        $phep=$value->post('tinh');
        switch ($phep){
            case '1':
                $result=$num1+$num2;
                break;
            case '2':
                $result=($num1>$num2)?$num1-$num2:$num2-$num1;
                break;
            case '3':
                $result=$num1*$num2;
                break;
            case '4':
                $result=$num1/$num2;
                break;
        }
        return view('index',['result'=>'ket qua:'.$result, 'num1'=>$num1, 'num2'=>$num2]);
    }
}
