<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style>
        body {font-family: Arial, Helvetica, sans-serif;}
        form {border: 3px solid #f1f1f1;}

        input[type=text], input[type=password], select {
            width: 100%;
            padding: 12px 20px;
            margin: 8px 0;
            display: inline-block;
            border: 1px solid #ccc;
            box-sizing: border-box;
        }

        button {
            background-color: #4CAF50;
            color: white;
            padding: 14px 20px;
            margin: 8px 0;
            border: none;
            cursor: pointer;
            width: 100%;
        }

        button:hover {
            opacity: 0.8;
        }

        .cancelbtn {
            width: auto;
            padding: 10px 18px;
            background-color: #f44336;
        }

        .imgcontainer {
            text-align: center;
            margin: 24px 0 12px 0;
        }

        img.avatar {
            width: 40%;
            border-radius: 50%;
        }

        .container {
            padding: 16px;
        }

        span.psw {
            float: right;
            padding-top: 16px;
        }

        /* Change styles for span and cancel button on extra small screens */
        @media screen and (max-width: 300px) {
            span.psw {
                display: block;
                float: none;
            }
            .cancelbtn {
                width: 100%;
            }
        }
    </style>
</head>
<body>

<h2></h2>

<form action="/lab1" method="post">
    @csrf
    <div class="container">
        <label for="n1"><b>number 1</b></label>
        <input type="text" placeholder="Enter Number1" name="num" value="{{$num1??''}}" required>

        <label for="n2"><b>number 2</b></label>
        <input type="text" placeholder="Enter Number2" name="num2"  value="{{$num2??''}}" required>
        <label for="exampleFormControlSelect1"><b> phép tính</b></label>
        <select class="form-control" id="exampleFormControlSelect1" name="tinh">
            <option value="1">+</option>
            <option value="2">-</option>
            <option value="3">*</option>
            <option value="4">/</option>

        </select>
        <button type="submit">OK!</button>
    </div>

    <div class="container" style="background-color:#f1f1f1">
        {{$result??''}}
    </div>
</form>

</body>
</html>
