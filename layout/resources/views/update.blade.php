<!doctype html>
<html lang="en">
<head>
    <title>Title</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

</head>
<body>
<br>
<br>
<br>
<div class="container">
    <form action="/bai4/update/{{$data->id}}" method="post">
        @csrf
        <div class="form-group">
            <label for="exampleInputEmail1">name</label>
            <input type="text" name="name" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" value="{{$data->name}}">

        </div>

        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
    <br>
    <br>
    <br>


    <table class="table">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">name</th>
            <th scope="col">action</th>

        </tr>
        </thead>
        <tbody>
        @foreach($data1 as $value)
            <tr>
                <th scope="row">{{$value->id}}</th>
                <td>{{$value->name}}</td>
                <td><a href="/bai4/edit/{{$value->id}}"><i class="fas fa-edit"></i></a>|
                    <span><form method="POST" action="/admin/news/delete/{{$value->id}}" onsubmit="return ConfirmDelete( this )" style="display:inline-block">
                    @method('DELETE')
                            @csrf
                    <button type="submit"><i class="far fa-trash-alt"></i></button>
                </form></span></td>

            </tr>
        @endforeach
        </tbody>
    </table>
</div>






<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<script src="https://kit.fontawesome.com/8b7cae1338.js" crossorigin="anonymous"></script>
</body>
</html>
