<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    $drink=['vodka','Gin','Brandy'];
    return view('page',['name'=>'Vu Hong Son','day'=>'Friday','drink'=>$drink]);
});
Route::get('/bai4','categoryController@create');
Route::post('/bai4','categoryController@store');
Route::get('/bai4/edit/{id}','categoryController@edit');
Route::post('/bai4/update/{id}','categoryController@update');
Route::DELETE('/bai4/delete/{id}','categoryController@destroy');

