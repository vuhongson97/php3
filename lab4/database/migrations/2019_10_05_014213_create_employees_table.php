<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('employees', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->string('contact_number');
            $table->timestamps();
        });
//        $fake=Faker\factory::create();
//        $limit=33;
//        for($i=0;$i<$limit;$i++){
//            DB::table('employees')->insert([
//                'name'=>$fake->name,
//                'email'=>$fake->email,
//                'contact_number'=>$fake->phoneNumber,
//            ]);
//
//        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employees');
    }
}
